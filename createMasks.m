% % create mask for each facial component
function masks = createMasks(I, landmarks, lm_pos)

names_groups = fieldnames(lm_pos);
n_groups = numel(names_groups);

masks.total_inner = zeros(size(I));

for ng = 1:n_groups
    x = eval(['landmarks(lm_pos.' names_groups{ng} ', 1);']);
    y = eval(['landmarks(lm_pos.' names_groups{ng} ', 2);']);
    eval(['masks.' names_groups{ng} ' = roipoly(I, x, y);']);
    
    masks.total_inner = masks.total_inner + eval(['masks.' names_groups{ng}]);
end

masks.total_outter = bwmorph(masks.total_inner, 'thicken', 30);

% find border
[gx,gy] = gradient(double(masks.total_outter));
masks.total_outter_border = (masks.total_outter > 0) & ((gx.^2+gy.^2)>0);