clear; close all;
addpath(genpath('.'));

% % Landmarks labels

% AAsthana
lm_aasthana.LeftEyebrow = 18:22;
lm_aasthana.RightEyebrow = 23:27;
lm_aasthana.LeftEye = 38:40;
lm_aasthana.RightEye = 43:48;
lm_aasthana.Nose = [28, 32, 36]; % 28:31;
% lm_aasthana.Nostrils = 32:36;
lm_aasthana.OutterMouth = 49:60;
% lm_aasthana.InnerMouth = 61:66;
lm_aasthana.Chin = 1:17;

lm_pos = lm_aasthana;

%------------------------------------------------%
% Choose Face Detector (0: Tree-Based Face Detector (p204), 1: Matlab Face Detector)
% Tree-Based Face Detector is much more accurate and suited for wild faces.
% But it is EXTREMELY slow!!
bbox_method = 1;
%------------------------------------------------%
% Choose Fitting Views (0: Two-Level and Multi-View Fitting, 1: Two-Level and Single-View Fitting)
% Default : select_nview = 1 (suited for near-frontal faces)
select_nview = 0;
%------------------------------------------------%
% Choose Fitting Method (1: DFRM, 2: GFRM-PO, 3: GFRM-Alternating
% Default : select_fitting = 1
% Note: For select_fitting = 3 (GFRM-Alternating), the model takes 10-15
% second to load.
select_fitting = 1;
%------------------------------------------------%
% Choose Visualize (0: Do Not Display Fitting Results, 1: Display Fitting Results and Pause of Inspection)
visualize=1;
%------------------------------------------------%


%------------------------------------------------%
% Load Test Images
image_path='./test_images/';
img_list=dir([image_path,'*.jpg']);
% img_list=[img_list;dir([image_path,'*.png'])];
data = [];
for i=1:size(img_list,1)
    data(i).name = img_list(i).name;
    data(i).img = im2double(imread([image_path,img_list(i).name]));
    data(i).points = []; % MAT containing 66 Landmark Locations
end
%------------------------------------------------%
%Run Demo
if select_fitting==1
    
    clm_model='model/DFRM_Model.mat';
    load(clm_model);    
    data=DFRM(clm_model,data,bbox_method,visualize);    
    
elseif select_fitting==2

    clm_model='model/GFRM_PO_Model.mat';
    load(clm_model);    
    data=GFRM_PO(CLM,grmf,data,bbox_method,select_nview,visualize);    
    
elseif select_fitting==3

    clm_model='model/GFRM_Alternating_Model.mat';
    disp('Loading Model...');pause(0.2);
    load(clm_model);    
    data=GFRM_Alt(CLM,grmf,data,bbox_method,select_nview,visualize);    
end


% % Perform Fitting
for i=1:size(data,2)
    
    landmarks = data(i).points;

    % % Load Image
    test_image=im2double(imread([image_path,img_list(i).name]));
    figure1=figure;
    imshow(test_image);hold on;
    
    if size(test_image,3) == 3
        test_input_image = im2double(rgb2gray(test_image));
    else
        test_input_image = im2double((test_image));
    end
       
%     landmarks = zeros(size(test_image,1), size(test_image,2));
%     rows = round(test_points(:,2));
%     cols = round(test_points(:,1));
%     idx = rows + (cols-1)*size(test_image,1);
%     landmarks(idx) = 1;
%     imwrite(landmarks, strcat(image_path, strrep(img_list(i).name, '.jpg', ''), '_landmarks.jpg'));
    
    % create masks for each facial component and draw limits
    masks = createMasks(test_input_image, landmarks, lm_pos);  
    [cont_i, cont_j] = find(masks.total_outter_border);
    plot(cont_j, cont_i, 'b.')

    plot(landmarks(:,1),landmarks(:,2),'g*','MarkerSize',3);hold off;
    legend('Initialization','Final Fitting');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]);
    saveas(figure1,[image_path,strrep(img_list(i).name, '.jpg', '_thInf.jpg')]);
    pause;
    close all;
    
end
